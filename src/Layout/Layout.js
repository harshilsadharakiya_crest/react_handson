import React, { Component } from 'react';
import {Layout, Menu, Button} from 'antd';
import {Route, NavLink} from 'react-router-dom';
import {connect} from 'react-redux';
import MediaQuery from 'react-responsive';
import Config from '../Config/Config';
import "./Layout.scss";

import {
  SettingOutlined,
  ContainerOutlined,
  MenuUnfoldOutlined,
  MenuFoldOutlined,
} from '@ant-design/icons';

const { SubMenu } = Menu;
const { Content, Sider } = Layout;

class LayoutComponent extends Component {

  state = {
    collapsed: false,
  };

  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render () {
    const menu = (
      <Menu
        theme="dark"
        mode="inline"
        style={{ height: '100%', borderRight: 0 }}
      >
        <SubMenu key="current" title="Current Config" icon={<SettingOutlined/>}>
          {this.props.currentConfig ? Object.keys(this.props.currentConfig).map((config) => {
            return (
              <Menu.Item key={"current" + config}>
                <NavLink to={{
                  pathname: '/current/' + config,
                }}>
                  {config}
                </NavLink>
              </Menu.Item>
            )}) : null}
        </SubMenu>
        <SubMenu key="create" title="Create Config" icon={<ContainerOutlined/>}>
          {Object.keys(this.props.createConfig).map((config) => {
            return (
              <Menu.Item key={"create" + config}>
                <NavLink to={{
                  pathname: '/create/' + config,
                }}>
                  {config}
                </NavLink>
              </Menu.Item>
          )})}
        </SubMenu>
      </Menu>
    );

    return (
      <Layout className="layout">
        <MediaQuery minWidth={500}>
          <Sider width={175}>
            {menu}
          </Sider>
        </MediaQuery>
        <MediaQuery maxWidth={499}>
          <Sider width={175} collapsed={this.state.collapsed}>
            <Button type="primary" onClick={this.toggleCollapsed} className="button">
              {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined)}
            </Button>
            {menu}
          </Sider>
        </MediaQuery>
        <Layout>
          <Content
            className="site-layout-background content"
          >
            <Route path="/current/:config" render={(props) => <Config type='current' {...props}/>}/>
            <Route path="/create/:config" render={(props) => <Config type='create' {...props}/>}/>
          </Content>
        </Layout>
      </Layout>
    );
  }
}

const mapStateToProps = state => {
  return {
    createConfig: state.createConfig,
    currentConfig: state.currentConfig
  }
};

export default connect(mapStateToProps)(LayoutComponent);
