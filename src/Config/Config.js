import React, { Component } from 'react';
import { Form, Input, Button, notification } from 'antd';
import { connect } from 'react-redux';

class Config extends Component {

  formRef = React.createRef();
  config = this.props.match.params.config;
  defaultValue = {};

  onFinish = (type, values) => {
    this.props.onCreate(type, values);
    let jsonData = JSON.stringify(values);
    console.log(jsonData);
    if (this.props.type === 'create'){
      notification['success']({message: type + ' config is successfully created'});
    } else {
      notification['success']({message: type + ' config is successfully saved'});
    }
  };

  onReset = () => {
    this.formRef.current.resetFields();
  }

  componentDidMount () {
    if (Object.keys(this.defaultValue).length) {
      this.formRef.current.setFieldsValue(this.defaultValue);
    }
    if (this.config in this.props.currentConfig && this.props.type === 'current') {
      this.formRef.current.setFieldsValue(this.props.currentConfig[this.config]);
    }
    this.defaultValue = {};
  }

  componentDidUpdate () {
    this.formRef.current.setFieldsValue(this.defaultValue);
    if (this.config in this.props.currentConfig && this.props.type === 'current') {
      this.formRef.current.setFieldsValue(this.props.currentConfig[this.config]);
    }
    this.defaultValue = {};
  }

  render() {
    this.config = this.props.match.params.config;
    const layout = {
      labelCol: {
        span: 7,
      },
      wrapperCol: {
        span: 9,
      },
    };

    const tailLayout = {
      wrapperCol: {
        offset: 7
      },
    };
    
    let form = ( this.config && (this.props.type === 'current' ? this.config in this.props.currentConfig : true) ?
      <Form 
        {...layout} 
        ref={this.formRef} 
        onFinish={(values) => this.onFinish(this.config, values)}>
        { this.props.createConfig[this.config].map((config) => {
          this.defaultValue[config.key] = config.defaultValue
          return (
            <Form.Item
              key={config.key}
              name={config.key}
              label={config.label}
              rules={[
                {
                  required: config.mandatory,
                  message: config.label + ' is required'
                },
                {
                  pattern: new RegExp(config?.validation),
                  message: config.label + ' is not valid'
                }
              ]}>
              {config.type !== 'password' ? <Input type={config.type} placeholder={config.placeholder} /> : 
                <Input.Password placeholder={config.placeholder} />}
            </Form.Item>
          )
        })}
        <Form.Item {...tailLayout} style={{marginBottom: 0}}>
          <Button type="primary" htmlType="submit">
            {this.props.type === 'create' ? 'Submit' : 'Save'}
          </Button>
          {this.props.type === 'create' ? <Button htmlType="button" onClick={this.onReset} style={{marginLeft: 10}}>
            Reset
          </Button> : null }
        </Form.Item>
      </Form> : null
    );

    return (
      <div>
        {form}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    currentConfig: state.currentConfig,
    createConfig: state.createConfig
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onCreate: (config, values) => dispatch({type: 'createConfig', config: config, values: values}),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Config);