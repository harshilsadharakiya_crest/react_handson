import AD_CONFIG from '../config_files/ad_config.json';
import LOG_CONFIG from '../config_files/logs_config.json';
import EMAIL_CONFIG from '../config_files/email_config.json';
import GDRIVE_CONFIG from '../config_files/gdrive_config.json';

const initialState = {
  createConfig: {
    Ad: AD_CONFIG,
    Email: EMAIL_CONFIG,
    Logs: LOG_CONFIG,
    Gdrive: GDRIVE_CONFIG
  },
  currentConfig: {}
};

const reducer = (state = initialState, action) => {
  if (action.type === 'createConfig') {
    const type = action.config
    const values = action.values
    const object = {}
    object[type] = values
    return {
      ...state,
      currentConfig: {
        ...state.currentConfig,
        ...object
      }
    }
  }
  return state
}

export default reducer;